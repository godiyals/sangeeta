package com.alfresco.api.example;

import java.io.IOException;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;

import com.alfresco.api.example.model.SiteEntry;
import com.alfresco.api.example.model.SiteList;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;

/**
 * Simple example that shows how to hit the Alfresco Public API with
 * the REST API to find the user's home network and to list up to
 * 10 sites visible to the user.
 *
 * @author sangeeta
 */
public class GetSitesExample extends BaseOnPremExample {

	 public static void main(String[] args) {
        GetSitesExample gse = new GetSitesExample();
        gse.doExample();
    }

    public void doExample() {
        try {
            // Find the user's home network
            String homeNetwork = getHomeNetwork();

            // List some of the sites the user can see
            GenericUrl sitesUrl = new GenericUrl(getAlfrescoAPIUrl() +
                                                 homeNetwork +
                                                 SITES_URL + "?maxItems=10");
            HttpRequest request = getRequestFactory().buildGetRequest(sitesUrl);
            SiteList siteList = request.execute().parseAs(SiteList.class);
            System.out.println("Up to 10 sites you can see are:");
            for (SiteEntry siteEntry : siteList.list.entries) {
                    System.out.println("**********************"+siteEntry.entry.id);
                    String rootFolderId = getRootFolderId(siteEntry.entry.id);
                    if(rootFolderId.equalsIgnoreCase("nextgen")){
                        System.out.println("===in if");
                    	Folder subFolder = createFolder(rootFolderId, getFolderName());

                        // Create a test document in the subFolder
                        Document document = createDocument(subFolder, getLocalFile(), getLocalFileType(), null);

                        System.out.println("=========================document  ::"+document.getName());
                    }
                    Folder subFolder = createFolder(rootFolderId, getFolderName());

                    // Create a test document in the subFolder
                    Document document = createDocument(subFolder, getLocalFile(), getLocalFileType(), null);

                    

            }

           
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        System.out.println("Done!");
    }
}
