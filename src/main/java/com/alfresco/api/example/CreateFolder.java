package com.alfresco.api.example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.Action;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisUnauthorizedException;

public class CreateFolder {
	
	public Folder createFolder(Session session,String folderName){
		Folder newFolder=null;
		try{
			// Create a new folder in the root folder

			Folder rootFolder = session.getRootFolder();

		    // Make sure the user is allowed to create a folder under the root folder
		    if (rootFolder.getAllowableActions().getAllowableActions().contains(Action.CAN_CREATE_FOLDER) == false) {
		        throw new CmisUnauthorizedException("Current user does not have permission to create a sub-folder in " +
		        		rootFolder.getPath());
		    }
		    // Check if folder already exist, if not create it
		    newFolder = (Folder) getObject(session, rootFolder, folderName);
		    if (newFolder == null) {
		        Map<String, Object> newFolderProps = new HashMap<String, Object>();
		        newFolderProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
		        newFolderProps.put(PropertyIds.NAME, folderName);
		        newFolder = rootFolder.createFolder(newFolderProps);

		        /*logger.info("Created new folder: " + newFolder.getPath() +
		                " [creator=" + newFolder.getCreatedBy() + "][created=" +
		                date2String(newFolder.getCreationDate().getTime()) + "]");*/
		    } else {
		      System.out.println("Folder already exist: " + newFolder.getPath());
		    }

		    return newFolder;
		}catch(Exception e){
			System.out.println("======================"+e.getStackTrace());
			 return newFolder;
		}

    }

	
	private  CmisObject getObject(Session session, Folder rootFolder, String objectName) {
	    CmisObject object = null;

	    try {
	    	System.out.println("===rootFolder.getPath();======="+rootFolder.getName());
	    	System.out.println("===rootFolder.getPath();======="+rootFolder.getPath());
	        String path2Object = rootFolder.getPath();
	        if (!path2Object.endsWith("/")) {
	            path2Object += "/";
	        }
	        path2Object += objectName;
	        System.out.println("====path2Object ::"+path2Object);
	        object = session.getObjectByPath(path2Object);
	    } catch (CmisObjectNotFoundException nfe0) {
	        // Nothing to do, object does not exist
	    }

	    return object;
	}

	public  String getDocumentPath(Document document) {
	    String path2Doc = getParentFolderPath(document);
	    if (!path2Doc.endsWith("/")) {
	        path2Doc += "/";
	    }
	    path2Doc += document.getName();
	    return path2Doc;
	}
	
	public  String getParentFolderPath(Document document) {
	    Folder parentFolder = getDocumentParentFolder(document);
	    return parentFolder == null ? "Un-filed" : parentFolder.getPath();
	}
	public  Folder getDocumentParentFolder(Document document) {
	    // Get all the parent folders (could be more than one if multi-filed)
	    List<Folder> parentFolders = document.getParents();
	    System.out.println("====parentFolders====="+parentFolders);
	    // Grab the first parent folder
	    if (parentFolders.size() > 0) {
	        if (parentFolders.size() > 1) {
	          //  logger.info("The " + document.getName() + " has more than one parent folder, it is multi-filed");
	        }

	        return parentFolders.get(0);
	    } else {
	      //  logger.info("Document " + document.getName() + " is un-filed and does not have a parent folder");
	        return null;
	    }
	}
	public  Folder CreateSubFolder_test(Session session,String sourceFolder,String subFolderName) { 
		 Folder subFolder = null;
		try{
			System.out.println("=======CreateSubFolder================");
		    Folder root = session.getRootFolder();
		   
      	     
		    ItemIterable<CmisObject> contentItems = root.getChildren();
		    for (CmisObject contentItem : contentItems) {
		    	
		        if (contentItem instanceof Document) {
		            Document docMetadata = (Document) contentItem;
		            ContentStream docContent = docMetadata.getContentStream();
		           System.out.println(docMetadata.getName() + " [size=" + docContent.getLength() + "][Mimetype=" +
		                    docContent.getMimeType() + "][type=" + docMetadata.getType().getDisplayName() + "]");
		        }
		        else if(contentItem instanceof Folder){
		        	
		                 Folder fdname = (Folder) contentItem;
		                 System.out.println("=====fbname============"+fdname.getName());
		                 if(fdname.getName().equalsIgnoreCase(sourceFolder))
		           	 	 System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		                 Map props = new HashMap();
		           	     props.put("cmis:objectTypeId","cmis:folder");
		           	     props.put("cmis:name", subFolderName);
		           	     subFolder = fdname.createFolder(props);
		           	  
		        }
		        
		        else {
		        	
		        	
		        	 System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		        	 
		        }
		       
		    }
		    return subFolder;
		}catch(Exception e){
			System.out.println("Folder already exist: ");
			 return subFolder;
		}
		
	}

public  Folder CreateSubFolder(Session session,Folder parentFolder,String subFolderName) { 
	try{
		System.out.println("=======CreateSubFolder================");
	    Folder root = session.getRootFolder();
	    Folder newSubFolder = (Folder) getObject(session, parentFolder, subFolderName);
	    if (newSubFolder == null) {
	        Map<String, Object> newFolderProps = new HashMap<String, Object>();
	        newFolderProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
	        newFolderProps.put(PropertyIds.NAME, subFolderName);
	        newSubFolder = parentFolder.createFolder(newFolderProps);
	    	System.out.println("newSubFoldernewSubFolder ::"+newSubFolder.getPath());

	        /*logger.info("Created new folder: " + newFolder.getPath() +
	                " [creator=" + newFolder.getCreatedBy() + "][created=" +
	                date2String(newFolder.getCreationDate().getTime()) + "]");*/
	    } else {
	      System.out.println("Folder already exist: " + newSubFolder.getPath());
	    }
	    return newSubFolder;
	}
	catch(Exception e){
		e.printStackTrace();
		return null;
		
	}

 	     
	    /*ItemIterable<CmisObject> contentItems = root.getChildren();
	    for (CmisObject contentItem : contentItems) {
	    	
	        if (contentItem instanceof Document) {
	            Document docMetadata = (Document) contentItem;
	            ContentStream docContent = docMetadata.getContentStream();
	           System.out.println(docMetadata.getName() + " [size=" + docContent.getLength() + "][Mimetype=" +
	                    docContent.getMimeType() + "][type=" + docMetadata.getType().getDisplayName() + "]");
	        }
	        else if(contentItem instanceof Folder){
	        	
	                 Folder fdname = (Folder) contentItem;
	                 System.out.println("=====fbname============"+fdname.getName());
	                 if(fdname.getName().equalsIgnoreCase(sourceFolder))
	           	 	 System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
	                 Map props = new HashMap();
	           	     props.put("cmis:objectTypeId","cmis:folder");
	           	     props.put("cmis:name", subFolderName);
	           	     subFolder = fdname.createFolder(props);
	           	  
	        }
	        
	        else {
	        	
	        	
	        	 System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
	        	 
	        }
	       
	    }
	    return subFolder;
	}catch(Exception e){
		System.out.println("Folder already exist: ");
		 return subFolder;
	}*/
	
}
}

